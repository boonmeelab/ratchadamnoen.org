/**
 * log utility
 */
function Logger() {}

exports.debug = console.dir;
exports.info  = console.log;
exports.error = console.error;
exports.warn  = console.warn;
