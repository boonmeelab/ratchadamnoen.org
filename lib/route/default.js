var qs = require('querystring');
var config = require('../config');
var util = require('../util');


exports.register = function(app) {

  app.get('/', function(req, res) {
    res.render('top');
  });

  app.get('/vis', function(req, res) {
    res.redirect('vis/coup');
  });

  app.get('/vis/coup', function(req, res) {
    res.render('vis/coup');
  });

  app.get('/about', function(req, res) {
    res.render('about');
  });

}
