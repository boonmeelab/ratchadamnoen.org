var crypto = require('crypto');
var fs = require('fs');
var libpath = require('path');
var config = require('./config');

const HASH_ASSET_SALT = 'f7e6d074c5ec4bba763ba1be6d67ec0026208500';

exports.hashAsset = function(str) {
  return crypto.createHash('md5').update(str + '-' + HASH_ASSET_SALT).digest("hex");
};

exports.checkHash = function(str) {
  return /^[0-9a-f]{32}$/.test(str);
};

exports.isFile = function(path, cb) {
  fs.stat(path, function(err, stats) {
    if (err) return cb(err);
    cb(null, stats.isFile());
  });
};

exports.isDirectory = function(path, cb) {
  fs.stat(path, function(err, stats) {
    if (err) return cb(err);
    cb(null, stats.isDirectory());
  });
};

exports.version = function(path) {
  var version = config.version || '';
  if (path) {
    if (version && path[0]=='/') version = '/' + version;
    version = libpath.join(version, path);
    // if (path[0]=='/') version = '/' + version;
  }
  return version;
};

/**
 * apply cache header
 */
exports.cache = function(res, minutes) {
  res.removeHeader('Pragma');
  res.set('Cache-Control','max-age='+minutes*60);
  res.set('Expires', new Date(Date.now()+minutes*60000));
};

exports.nocache = function(req, res, next) {
  res.set('Pragma','no-cache');
  res.set('Cache-Control','no-cache');
  res.set('Expires', expire);
  res.removeHeader('X-Powered-By');
  next();
};

/**
 * send error content to the client
 * @param res
 * @param err
 * @param status
 */
exports.sendError = function(res, err, status) {
  status = status ? status : 500;
  if ('status' in err) {
    status = err.status;
  }
  res.json({
    code: err.code,
    message: err.message,
    status: status
  }, status);
};
